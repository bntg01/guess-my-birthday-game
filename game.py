from random import randint

name = input("Hi! What is your name? ")

for guess_num in range(5):
    print("Guess", guess_num + 1, ":", name, "were you born in", randint(1,12), "/", randint(1924, 2004))
    guess = input("yes or no? ")
    if guess == "yes":
        print("I knew it!")
        break
    elif guess == "no" and guess_num < 4:
        print("Drat! Lemme try again!")
    elif guess == "no" and guess_num == 4:
        print("I have other things to do. Good bye.")
    else:
        print("Bad input, try again.")
